﻿using MoneyControlApp.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyControlApp.Domain.Models
{
    public class PlannedPayment : BaseEntity
    {
        public PaymentType Type { get; set; }

        public PaymentCategory Category { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime PlannedPaymentDate { get; set; }

        public string Sender { get; set; }

        public string Recipient { get; set; }

        public float Amount { get; set; }

        public bool IsFrequency { get; } = true;
    }
}
