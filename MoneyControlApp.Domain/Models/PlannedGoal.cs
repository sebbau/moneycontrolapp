﻿using MoneyControlApp.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyControlApp.Domain.Models
{
    public class PlannedGoal : BaseEntity
    {
        public GoalType Type { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Year { get; set; }

        public byte? Month { get; set; }

        public string Quarter { get; set; }

        public GoalStatus Status { get; set; } = GoalStatus.Unconfirmed;

        public bool IsFrequency { get; set; } = true;
    }
}
