﻿using MoneyControlApp.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyControlApp.Domain.Models
{
    public class Goal : BaseEntity
    {
        public GoalType Type { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Year { get; set; }

        public byte? Month { get; set; }

        public string Quarter { get; set; }

        public GoalStatus Status { get; set; }

        public bool IsFrequency { get; set; }

        public GoalFrequency? Frequency { get; set; }

        public DateTime? FrequencyEndDate { get; set; }
    }
}
