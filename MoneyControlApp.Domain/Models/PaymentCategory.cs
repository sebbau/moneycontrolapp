﻿using MoneyControlApp.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyControlApp.Domain.Models
{
    public class PaymentCategory : BaseEntity
    {
        public PaymentType PaymentType { get; set; }

        public string Name { get; set; }
    }
}
