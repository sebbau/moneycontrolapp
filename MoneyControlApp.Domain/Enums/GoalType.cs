﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyControlApp.Domain.Enums
{
    public enum GoalType
    {
        Deposit = 1,
        Withdraw,
        Saving
    }
}
