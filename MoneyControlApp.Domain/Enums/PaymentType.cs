﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyControlApp.Domain.Enums
{
    public enum PaymentType
    {
        Deposit = 1,
        Withdraw
    }
}
