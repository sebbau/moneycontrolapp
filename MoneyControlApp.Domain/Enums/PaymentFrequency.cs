﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyControlApp.Domain.Enums
{
    public enum PaymentFrequency
    {
        Daily = 1,
        Weekly,
        Monthly,
        Quarterly
    }
}
