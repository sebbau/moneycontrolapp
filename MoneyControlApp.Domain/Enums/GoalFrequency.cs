﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyControlApp.Domain.Enums
{
    public enum GoalFrequency
    {
        Monthly = 1,
        Quarterly,
        Yearly
    }
}
