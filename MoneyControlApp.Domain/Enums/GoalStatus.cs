﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyControlApp.Domain.Enums
{
    public enum GoalStatus
    {
        Unconfirmed = 1,
        Confirmed
    }
}
